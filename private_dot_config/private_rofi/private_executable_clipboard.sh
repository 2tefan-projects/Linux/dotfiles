#!/bin/sh

if [ "$DISPLAY_SERVER" = "wayland" ]; then
	if [ -z "$1" ]; then
		cliphist list
	else
		echo "$1" | cliphist decode | wl-copy
	fi
else
	if [ -z "$1" ]; then
		greenclip print
	else
		greenclip print "$1"
	fi
fi

