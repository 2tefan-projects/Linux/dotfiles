#!/bin/sh -x

. $PERSONAL_CONFIG/env.sh

xrandr --output $MONITOR_1 --primary
nvidia-settings --assign CurrentMetaMode="$MONITOR_EXT_1: 1368x912_60.00 +0+0,
                                          $MONITOR_1: 2560x1440_240 +3288+0,
                                          $MONITOR_2: 1920x1080_60 +1368+0,
                                          $MONITOR_3: nvidia-auto-select +5848+0 {rotation=left}"
reload_desktop

