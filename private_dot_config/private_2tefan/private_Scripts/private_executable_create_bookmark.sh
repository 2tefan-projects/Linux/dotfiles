#!/bin/sh
# Usage: `create_bookmark.sh <path to desired folder> <name of bookmark>`
# Access with `goto @<name of bookmark>`

set -eu

ln -s "$(realpath ${1})" "${XDG_CONFIG_HOME:-$HOME/.config}/Bookmarks/@${2}"
