#!/bin/bash

trap "exit" INT TERM ERR
trap "kill 0" EXIT

i3-msg "workspace 13; append_layout ~/.config/i3/layouts/yash2x3.json"
i3-msg "gaps inner current set 0; gaps outer current set 0; gaps top current set 30"

xfce4-terminal -H --disable-server -x "fortune" &
parallel --header : --line-buffer --tag godot --path ~/dev/godot/yash --args "--name={name} --join=localhost:2021" ::: name Ada Bernie Cindy Dena Erik

wait
