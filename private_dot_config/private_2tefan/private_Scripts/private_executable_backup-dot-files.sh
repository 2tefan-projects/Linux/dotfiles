#!/bin/bash

if ! command -v chezmoi &>/dev/null; then
    echo "Chemzoi could not be found" >&2
    exit
fi

chmod 700 $HOME/.config -R

filesToBackupHome=(
    .xprofile
)

filesToBackupXDG_CONFIG_HOME=(
    yprofile
    Xresources
    #gtk-2.0/gtkfilechooser.ini
    gtk-2.0/gtkrc-2.0
    gtk-3.0/bookmarks
    gtk-3.0/settings.ini
    pulse/daemon.conf
    #mimeapps.list
    user-dirs.dirs
    user-dirs.locale

    i3/config
    plank
    ulauncher
    picom
    polybar
    nvim/init.vim
    picom

    xfce4/panel
    xfce4/terminal/terminalrc
    xfce4/xfconf/xfce-perchannel-xml/displays.xml
    xfce4/xfconf/xfce-perchannel-xml/keyboard-layout.xml
    xfce4/xfconf/xfce-perchannel-xml/keyboards.xml
    xfce4/xfconf/xfce-perchannel-xml/pointers.xml
    xfce4/xfconf/xfce-perchannel-xml/xfce4-keyboard-shortcuts.xml
    xfce4/xfconf/xfce-perchannel-xml/xfce4-notifyd.xml
    xfce4/xfconf/xfce-perchannel-xml/xfce4-panel.xml
    xfce4/xfconf/xfce-perchannel-xml/xfce4-power-manager.xml
    xfce4/xfconf/xfce-perchannel-xml/xfce4-screensaver.xml
    xfce4/xfconf/xfce-perchannel-xml/xfwm4.xml
    xfce4/xfconf/xfce-perchannel-xml/xsettings.xml

    onedrivePersonal/config
    onedriveSchool/config
    systemd/user/multi-user.target.wants/onedrive-personal.service
    systemd/user/multi-user.target.wants/onedrive-school.service

    2tefan
    screenlayout
)

add() {
    for i in $@; do
        if [[ $1 == $i ]]; then
            continue
        fi

        local FILE="$1/$i"
        chmod 700 $FILE -R
        chezmoi add $FILE -r
    done
}

add $XDG_CONFIG_HOME "${filesToBackupXDG_CONFIG_HOME[@]}"
add $HOME "${filesToBackupHome[@]}"

chezmoi forget $HOME/.config/ulauncher/ext_preferences/*
