#!/bin/sh

set -eu

for dir in *; do current_dir=$(pwd) && cd $dir && git fetch && git switch main && git pull ; cd $current_dir ; done
