# Bookmarks 📖
When using zsh with [2tefan/prezto](https://github.com/2tefan/prezto) it is possible to create bookmarks.

## Setup ⚙️

Use `create_bookmark.sh` script.

```sh
create_bookmark.sh <path to desired folder> <name of bookmark>
```

## Usage 🖊️

```sh
goto @<name of bookmark>
```
