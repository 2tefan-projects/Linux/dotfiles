#!/bin/sh

. "$PERSONAL_CONFIG/env.sh"

: "${CONFIG_FILE:=$XDG_CONFIG_HOME/polybar/config_v001.ini}"

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u "$(id -ru)" -x polybar >/dev/null; do sleep 1; done

# Launch Polybar, using default config location ~/.config/polybar/config
polybar --config="$CONFIG_FILE" main &
polybar --config="$CONFIG_FILE" main_b &
polybar --config="$CONFIG_FILE" 2nd &
polybar --config="$CONFIG_FILE" 2nd_b &
polybar --config="$CONFIG_FILE" 3rd &
polybar --config="$CONFIG_FILE" 4th &
polybar --config="$CONFIG_FILE" vm-surface &

echo "Polybar launched..."

