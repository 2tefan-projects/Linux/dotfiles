-- Hi, this is my WIP nvim config
-- Have fun ;)

-- So I don't always have to type that piece of bollocks out
local o = vim.opt
local g = vim.g


-- Fancy lazy plugin manager for nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
o.rtp:prepend(lazypath)

-- Set some defaults
--- Set numbers on left side
o.number = true

--- Use tabs with size of 4 spaces
o.expandtab = false -- tabs 4tw
o.smartindent = true
o.tabstop = 4
o.shiftwidth = 4

--- Display spaces & tabs
o.list = true
o.lcs = o.lcs + 'space:·'

g.ale_completion_enabled = 1
g.ale_completion_autoimport = 1


-- Setup plugins
require("lazy").setup("plugins")
require("keybinds")

